第一题：Maven工程

刘荏栎（组长）：https://gitee.com/rioblu/First-Maven-Project.git
张新尚：https://gitee.com/xiaoxin1998/day02-Exercise1.git
汤淦铭：https://gitee.com/tang_ganming/webapp.git
李烁波：https://gitee.com/li_shuobo/maven.git
陈俊宏：https://gitee.com/junhong_chen/day02_web.git

第二题：多模块工程

刘荏栎（组长）：https://gitee.com/rioblu/MultipleModuleProject.git
张新尚：https://gitee.com/xiaoxin1998/day02-Exercise2.git
汤淦铭：https://gitee.com/tang_ganming/Multi-Module-Maven-Project.git
李烁波：https://gitee.com/li_shuobo/multimodule_maven.git
陈俊宏：https://gitee.com/junhong_chen/day02_multiModule.git