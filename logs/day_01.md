#### 复习javaEE相关概念
#### 学习使用git和码云的相关操作
1. 在码云创建一个仓库
2. 在码云fork一个仓库
3. 在码云提交PR、PR如何修改
4. 在码云提交Issues
5. 在码云如何管理一个仓库：添加仓库管理员
6. 使用git命令行创建仓库
7. 在idea里初始化git仓库；commit操作；push操作；
8. 在idea里如何clone一个git远程仓库
9. git stash在idea中的使用（暂存修改）

#### 作业
1. 分小组完成。小组每个成员独立创建一个git仓库，并通过码云互相提交PR；
2. 分小组完成。小组长创建一个git仓库，并把小组的其它成员设置为仓库开发者。其它成员都向这个仓库push一次。
3. 分小组完成。小组长创建一个git仓库，并创建一个空白的txt文件，小组的各个成员都向这个文件的第一行录入一些随机字符并push。（学会如何在idea解决冲突）

#### 如何交作业
各小组长把各成员的作业仓库记录在一个txt文件，并PR到本课程仓库，目录是 作业/day_XX/小组长全名.txt