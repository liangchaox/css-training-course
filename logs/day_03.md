## 内容：
1. 学习Jar文件，包含Jar文件的编译、打包、结构分析等；[https://gitee.com/dgut-sai/jar-demo](https://gitee.com/dgut-sai/jar-demo)
2. 学习War文件；
3. JavaEE的简述
4. 在idea中演示整个JavaEE应用程序的制作过程。[https://gitee.com/dgut-sai/JavaEE-Maven-Demo](https://gitee.com/dgut-sai/JavaEE-Maven-Demo)
5. 学习使用Servlet 3.0的Jsonb API处理Json文档；
6. 学习使用H2数据库；
7. java 通过SPI加载数据库驱动原理；
8. 学习使用jdbc连接数据库；

### 作业
分小组，完成以下内容：
1. 新建一个git仓库
2. 完成一个简单的JavaEE应用程序开发
3. 通过curl发送一个post请求，传送json字符串到后端。（学生属性的json,可以参考本仓库的示例）
4. 后端程序接收到json字符串，通过jsonb的API转换为pojo或Map对象。（约定是学生实体的相关属性）
5. 接收到的数据通过jdbc连接H2数据库，保存到表。（构造一个学生属性的表）
6. 如果保存数据正确，返回一个json回前端：{"code":0,"msg":"操作成功"}